Name: Douglas Archibald
EID: dha338
GitLab ID: dharchibald
HackerRank ID: d_archibald84
Git SHA: 9d4559a7a1fd6861981c62d1542227bda6fcd19e
GitLab Pipelines: https://gitlab.com/dharchibald/cs373-collatz/pipelines
Estimated completion time: 5 hours
Actual completion time: 7 hours
Comments: Code structure derived from my own C++ Collatz project from CS 371P
