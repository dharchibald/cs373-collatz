#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

MAX_INPUT = 1000000
CYCLES = [0] * MAX_INPUT

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    assert 0 < i < MAX_INPUT
    assert 0 < j < MAX_INPUT

    # Input may be out of order
    if i < j:
        low = i
        high = j
    else:
        low = j
        high = i

    if ((high >> 1) + 1) > low:
        bound = (high >> 1) + 1
    else:
        bound = low

    max_cycle = 0
    for k in range(bound, high + 1):
        max_cycle = max(collatz_cycle(k), max_cycle)
    return max_cycle


# -------------
# collatz_cycle
# -------------


def collatz_cycle(num: int) -> int:
    global MAX_INPUT
    global CYCLES
    # Return immediately for base case
    if num == 1:
        return 1
    # Return immediately if cycles are cached
    if num < MAX_INPUT and CYCLES[num]:
        return CYCLES[num]

    old_num = num
    if num % 2 == 0:
        num = num >> 1
        cycles = collatz_cycle(num) + 1
    else:
        num = num + (num >> 1) + 1
        cycles = collatz_cycle(num) + 2
    # Cache cycle length for num
    if old_num < MAX_INPUT:
        CYCLES[old_num] = cycles

    return cycles


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
