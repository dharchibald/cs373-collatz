#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_3(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_4(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    def test_eval_5(self):
        v = collatz_eval(1000, 900)
        self.assertEqual(v, 174)

    def test_eval_6(self):
        v = collatz_eval(100000, 100500)
        self.assertEqual(v, 341)

    def test_eval_7(self):
        v = collatz_eval(700000, 750000)
        self.assertEqual(v, 504)

    def test_eval_8(self):
        v = collatz_eval(950000, 999999)
        self.assertEqual(v, 458)

    def test_eval_9(self):
        v = collatz_eval(1784, 5398)
        self.assertEqual(v, 238)

    def test_eval_10(self):
        v = collatz_eval(6771, 4399)
        self.assertEqual(v, 262)

    def test_eval_11(self):
        v = collatz_eval(12047, 16523)
        self.assertEqual(v, 276)

    def test_eval_12(self):
        v = collatz_eval(87034, 102877)
        self.assertEqual(v, 341)

    def test_eval_13(self):
        v = collatz_eval(5610, 200)
        self.assertEqual(v, 238)

    def test_eval_14(self):
        v = collatz_eval(890, 23)
        self.assertEqual(v, 179)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
